//
//  EzvizModel.swift
//  flutter_ezviz
//
//  Created by 江鴻 on 2019/8/24.
//

import Foundation
import EZOpenSDKFramework


let AppKey: String = "888b1f2a00d14aee910d1a7437669a2a"
let AccessToken: String = "at.1zvz23zq4ahu89r1cf1fmlv41s0h8g22-6t4r5o4vql-1hvx6ud-h0myuu18u"

let Action_START = "EZPTZAction_START"
let Action_STOP  = "EZPTZAction_STOP"
let Command_Left = "EZPTZCommand_Left"
let Command_Right = "EZPTZCommand_Right"
let Command_Up = "EZPTZCommand_Up"
let Command_Down = "EZPTZCommand_Down"
let Command_ZoomIn = "EZPTZCommand_ZoomIn"
let Command_ZoomOut = "EZPTZCommand_ZoomOut"

extension EZDeviceInfo {
    func toJSON()-> [String:Any] {
        return [
            "deviceSerial":self.deviceSerial,
            "deviceName":self.deviceName,
            "isSupportPTZ":self.isSupportPTZ,
            "cameraNum":self.cameraNum,
            "deviceCover":self.deviceCover,
            "deviceType":self.deviceType,
//            "isSupportTalk":self.isSupportTalk,
//            "isSupportZoom":self.isSupportZoom,
            "status":self.status,
        ]
    }
}
extension EZProbeDeviceInfo {
    func toJSON()-> [String:Any] {
        return [
        /// 展示名称
            "displayName":self.displayName,
        /// 设备大类名
            "category":self.category,
        /// 设备型号名
            "model":self.model,
        /// 设备短序列号
            "subSerial":self.subSerial,
        /// 设备长序列号
            "fullSerial":self.fullSerial,
        /// 设备在线状态，1-在线，其他-不在线
            "status":self.status,
        /// 设备图片
            "defaultPicPath":self.defaultPicPath,
        /// 是否支持wifi，0-不支持，1-支持，2-支持带userId的新的wifi配置方式，3-支持smartwifi
            "supportWifi":self.supportWifi,
        /// 是否支持AP配网，2-支持AP，其他值为不支持AP配网
            "supportAP":self.supportAP,
        /// 是否支持声波配置,0-不支持，1-支持
            "supportSoundWave":self.supportSoundWave,
        /// 是否支持云存储，0-不支持，1-支持
            "supportCloud":self.supportCloud,
        /// 设备协议版本
            "releaseVersion":self.releaseVersion,
        /// 设备版本
            "version":self.version,
        /// 可用于添加的通道数
            "availiableChannelCount":self.availiableChannelCount,
        /// N1，R1，A1等设备关联的设备数
            "relatedDeviceCount":relatedDeviceCount,
        /// 能力集
            "supportExt":self.supportExt
        ]
    }
}

extension EZHCNetDeviceInfo {
    func toJSON()-> [String:Any] {
        return [
            "userId":self.userId,
            "dChannelCount":self.dChannelCount,
            "dStartChannelNo":self.dStartChannelNo,
            "channelCount":self.channelCount,
            "startChannelNo":self.startChannelNo,
            "byDVRType":self.byDVRType,
        ]
    }
}

extension Notification.Name {
    static let EzvizPlayStatusChanged = Notification.Name("EzvizPlayStatusChanged")
}

var PTZKeys: [AnyHashable : Any] = [
    Action_START: EZPTZAction.start,
    Action_STOP: EZPTZAction.stop,
    Command_Left: EZPTZCommand.left,
    Command_Right: EZPTZCommand.right,
    Command_Up: EZPTZCommand.up,
    Command_Down: EZPTZCommand.down,
    Command_ZoomIn: EZPTZCommand.zoomIn,
    Command_ZoomOut: EZPTZCommand.zoomOut,
]

var netPTZKeys: [AnyHashable : Any] = [
    Action_START: EZPTZActionType.START,
    Action_STOP: EZPTZActionType.STOP,
    Command_Left: EZPTZCommandType.LEFT,
    Command_Right: EZPTZCommandType.RIGHT,
    Command_Up: EZPTZCommandType.UP,
    Command_Down: EZPTZCommandType.DOWN,
    Command_ZoomIn: EZPTZCommandType.ZOOM_IN,
    Command_ZoomOut: EZPTZCommandType.ZOOM_OUT,
]

/// 数据返回
struct EzvizPlayerEventResult : Codable {
    let eventType: String
    let msg: String
    let data: String?
    
    init(eventType: String, msg: String, data: String?) {
        self.eventType = eventType
        self.msg = msg
        self.data = data
    }
}

/// 播放状态
struct EzvizPlayerResult: Codable {
    let status: UInt
    let message: String?
    
    init(status: UInt,message: String?) throws {
        self.status = status
        self.message = message
    }
}
